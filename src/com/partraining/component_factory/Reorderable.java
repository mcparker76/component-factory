/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

/**
 * Interface for reordering elements
 * @author Matt Parker
 *
 */
public interface Reorderable {
	
	/**
	 * Reorders
	 * @param fromIndex
	 * @param toIndex
	 */
	void reorder(int fromIndex, int toIndex);

}
