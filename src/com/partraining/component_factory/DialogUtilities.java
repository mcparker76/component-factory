/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

import java.awt.Component;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.partraining.table_layout_utilities.TableLayoutUtilities;


/**
 * A utility class for displaying dialogs
 * @author Matt Parker
 *
 */
public class DialogUtilities {
	
	//TODO MCP 12/10/2022 higher resolution images
	private static final URL CHECK_MARK = DialogUtilities.class.getClassLoader().getResource("icons8-check-mark-48.png");
	private static final URL EXCLAMATION_MARK = DialogUtilities.class.getClassLoader().getResource("icons8-exclamation-mark-48.png");
	private static final URL QUESTION_MARK = DialogUtilities.class.getClassLoader().getResource("icons8-question-mark-48.png");
	private static final URL INFORMATION = DialogUtilities.class.getClassLoader().getResource("icons8-information-48.png");
	
	private DialogUtilities() {
		//no implementation
	}
		
	/**
	 * Displays an error dialog
	 * @param appName - the app name for the title
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 */
	public static void displayErrorDialog(String appName, String message, Component parent, URL icon) {
		JDialog dialog = createDialog(appName, message, parent, icon, EXCLAMATION_MARK);
		dialog.setVisible(true);
	}
	
	/**
	 * Displays an error dialog
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 */
	public static void displayErrorDialog(String message, Component parent, URL icon) {
		JDialog dialog = createDialog(null, message, parent, icon, EXCLAMATION_MARK);
		dialog.setVisible(true);
	}
	
	/**
	 * Displays a confirm dialog with YES/NO options. 
	 * Use JOptionPane.OK_OPTION to check if YES selected.
	 * @param appName - the app name for the title
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 * @return - confirm response
	 */
	public static int displayConfirmDialog(String appName, String message, Component parent, URL icon) {
		ConfirmDialog confirmDialog = new ConfirmDialog(appName, message, parent, icon);
		confirmDialog.setVisible(true);
		
		return confirmDialog.getValue();
	}
	
	/**
	 * Displays a confirm dialog with YES/NO options. 
	 * Use JOptionPane.OK_OPTION to check if YES selected.
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 * @return - confirm response
	 */
	public static int displayConfirmDialog(String message, Component parent, URL icon) {
		ConfirmDialog confirmDialog = new ConfirmDialog(null, message, parent, icon);
		confirmDialog.setVisible(true);
		
		return confirmDialog.getValue();
	}
	
	/**
	 * Displays a message dialog
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 */
	public static void displayMessageDialog(String message, Component parent, URL icon) {
		JDialog dialog = createDialog(null, message, parent, icon, INFORMATION);
		dialog.setVisible(true);
	}
	
	/**
	 * Displays a message dialog
	 * @param appName - the app name for the title
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 */
	public static void displayMessageDialog(String appName, String message, Component parent, URL icon) {
		JDialog dialog = createDialog(appName, message, parent, icon, INFORMATION);
		dialog.setVisible(true);
	}
	
	/**
	 * Displays a success message dialog
	 * @param appName - the app name for the title
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 */
	public static void displaySuccessDialog(String appName, String message, Component parent, URL icon) {
		JDialog dialog = createDialog(appName, message, parent, icon, CHECK_MARK);
		dialog.setVisible(true);
	}
	
	/**
	 * Displays a success message dialog
	 * @param message - the dialog message
	 * @param parent - the parent component
	 * @param icon - the url for image icon
	 */
	public static void displaySuccessDialog(String message, Component parent, URL icon) {
		JDialog dialog = createDialog(null, message, parent, icon, CHECK_MARK);
		dialog.setVisible(true);
	}
	
	
	private static JDialog createDialog(String title, String message, Component parent, URL icon, URL image) {
		JDialog dialog = new JDialog();
		
		if (title != null) {
			dialog.setTitle(title);		
		}
		dialog.setModal(true);
		dialog.setResizable(false);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		try {
			dialog.setIconImage(ImageIO.read(icon));
		} catch (IOException e1) {
			//NO OP
		}	
		
		JPanel panel = new JPanel(TableLayoutUtilities.createLayout(2,2));

		BufferedImage wPic = null;
		
		try {
			wPic = ImageIO.read(image);
		} catch (Exception e) {
			//NO OP
		}

		JLabel lblImageIcon = wPic == null ? new JLabel() : new JLabel(new ImageIcon(wPic));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(ComponentFactory.createButton("Close", new Runnable() {
				@Override
				public void run() {
					dialog.dispose();
				}
			}));
		
		panel.add(lblImageIcon, "1, 1, T, C");
		panel.add(new JLabel(message), "3, 1, T, C");
		panel.add(buttonPanel, "3, 3, T, C");
		
		dialog.getContentPane().add(panel);
		dialog.pack();

		if (parent != null) {
			Point parentLocation = parent.getLocation();
			int parentWidth = parent.getSize().width;
			int thisWidth = dialog.getSize().width;
			int offset = (parentWidth - thisWidth) / 2;
			Point location = new Point();
			location.y = parentLocation.y;
			location.x = parentLocation.x + offset;
			dialog.setLocation(location);	
		}
		
		return dialog;
	}
	
	private static class ConfirmDialog extends JDialog{
		
		private static final long serialVersionUID = -352017800290550018L;
		private boolean isYes;
		
		public ConfirmDialog(String title, String message, Component parent, URL icon) {
			if (title != null) {
				setTitle(title);		
			}
			
			setModal(true);
			setResizable(false);
			setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			
			try {
				setIconImage(ImageIO.read(icon));
			} catch (IOException e1) {
				//NO OP
			}	
			
			JPanel panel = new JPanel(TableLayoutUtilities.createLayout(2,2));

			BufferedImage wPic = null;
			
			try {
				wPic = ImageIO.read(QUESTION_MARK);
			} catch (Exception e) {
				//NO OP
			}

			JLabel lblImageIcon = wPic == null ? new JLabel() : new JLabel(new ImageIcon(wPic));
			
			JPanel buttonPanel = new JPanel();
				buttonPanel.add(ComponentFactory.createButton("Yes", createRunnable(true, this)));
			buttonPanel.add(ComponentFactory.createButton("No", createRunnable(false, this)));
			
			panel.add(lblImageIcon, "1, 1, T, C");
			panel.add(new JLabel(message), "3, 1, T, C");
			panel.add(buttonPanel, "3, 3, T, C");
			
			getContentPane().add(panel);

			if (parent != null) {
				pack();
				Point parentLocation = parent.getLocation();
				int parentWidth = parent.getSize().width;
				int thisWidth = getSize().width;
				int offset = (parentWidth - thisWidth) / 2;
				Point location = new Point();
				location.y = parentLocation.y;
				location.x = parentLocation.x + offset;
				setLocation(location);	
			}
		}
		
		public void setYes(boolean isYes) {
			this.isYes = isYes;
		}
		
		public int getValue() {
			return isYes ? 0 : 1;
		}
		
		private Runnable createRunnable(boolean isYes, ConfirmDialog dialog) {
			return new Runnable() {

				@Override
				public void run() {
					dialog.setYes(isYes);
					dialog.dispose();
				}
			};
		}
		
	}
}
