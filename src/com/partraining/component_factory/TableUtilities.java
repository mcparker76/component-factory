/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

import java.awt.event.MouseEvent;
import java.util.Set;

import javax.swing.DropMode;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Utility class for JTables
 * @author Matt Parker
 *
 */
public class TableUtilities {
	
	private TableUtilities() {
		//no implementation
	}
	
	/**
	 * Create a table where rows can be dragged and dropped
	 * @return - table
	 */
	public static JTable createDragDropTable() {
		JTable table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDragEnabled(true);
		table.setDropMode(DropMode.INSERT_ROWS);
		table.setTransferHandler(new TableRowTransferHandler(table));
		
		return table;
	}

	/**
	 * Create a JTable
	 * @param selectionMode - selection mode using static constants from @ListSelectionModel
	 * @return - table
	 */
	public static JTable createTable() {
		JTable table = createTable(-1, null, null);
		table.setRowSelectionAllowed(false);
		return table;
	}
	
	/**
	 * Creates a JTable 
	 * @param action 
	 * @return
	 */
	public static JTable createTable(Runnable action) {
		return createTable(0, action, null);
	}
	
	/**
	 * Creates a JTable
	 * @param selectionMode - the selectionMode using static constants from @ListSelectionModel
	 * @param action - the handler for table selection 
	 * @return - table
	 */
	public static JTable createTable(int selectionMode, Runnable action) {
		return createTable(selectionMode, action, null);
	}
	
	/**
	 * Creates a JTable
	 * @param selectionMode - the selectionMode using static constants from @ListSelectionModel
	 * @param action - the handler for table selection
	 * @param columnsWithToolTip - Set<@Integer> of columns with a tooltip
	 * @return - table
	 */
	public static JTable createTable(int selectionMode, Runnable action, Set<Integer> columnsWithToolTip) {
		JTable table = new JTable() {
			private static final long serialVersionUID = -8587023832722991739L;
		
			@Override
	        public String getToolTipText(MouseEvent e) {
	            String tip = null;
	            java.awt.Point p = e.getPoint();
	            int rowIndex = rowAtPoint(p);
	            int colIndex = columnAtPoint(p);

	            try {
	            	if (columnsWithToolTip != null && columnsWithToolTip.contains(colIndex)) {
	            		tip = getValueAt(rowIndex, colIndex).toString();
	            	}
	            } catch (RuntimeException e1) {
	                //catch null pointer exception if mouse is over an empty line
	            }

	            return tip;
	        }
		};

		if (action != null) {
			table.getSelectionModel().addListSelectionListener(e -> action.run());		
		}
	
		table.getTableHeader().setReorderingAllowed(false);
		
		if (selectionMode != -1) {
			table.setSelectionMode(selectionMode);
		}
		
		return table;
	}
	
	/**
	 * Creates a JTable
	 * @param columnsWithToolTip - Set<@Integer> of columns with a tooltip
	 * @return - table
	 */
	public static JTable createTable(Set<Integer> columnsWithToolTip) {
		JTable table = new JTable() {
			private static final long serialVersionUID = -8587023832722991739L;
		
			@Override
	        public String getToolTipText(MouseEvent e) {
	            String tip = null;
	            java.awt.Point p = e.getPoint();
	            int rowIndex = rowAtPoint(p);
	            int colIndex = columnAtPoint(p);

	            try {
	            	if (columnsWithToolTip != null && columnsWithToolTip.contains(colIndex)) {
	            		tip = getValueAt(rowIndex, colIndex).toString();
	            	}
	            } catch (RuntimeException e1) {
	                //catch null pointer exception if mouse is over an empty line
	            }

	            return tip;
	        }
		};
	
		table.getTableHeader().setReorderingAllowed(false);
		
		return table;
	}
	
	/**
	 * Creates a JTable
	 * @param columnsWithToolTip - Set<@ColumnWithTooltip> of columns with a tooltip
	 * @return - table
	 */
	public static JTable createTableWithHiddenColumnTooltip(Set<ColumnWithTooltip> columns) {
		JTable table = new JTable() {
			private static final long serialVersionUID = -8587023832722991739L;
		
			@Override
	        public String getToolTipText(MouseEvent e) {
	            String tip = null;
	            java.awt.Point p = e.getPoint();
	            int rowIndex = rowAtPoint(p);
	            int colIndex = columnAtPoint(p);

	            try {
	            	if (columns != null) {
	            		for (ColumnWithTooltip col : columns) {
	            			if (col.getColumn() == colIndex) {
	            				tip = getValueAt(rowIndex, col.getHiddenColumn()).toString();
	            				break;
	            			}
	            		}
	            	}
	            } catch (RuntimeException e1) {
	                //catch null pointer exception if mouse is over an empty line
	            }

	            return tip;
	        }
		};
	
		table.getTableHeader().setReorderingAllowed(false);
		table.setRowSelectionAllowed(false);
		
		return table;
	}
		
	/**
	 * Sets column width
	 * @param columnIndex - the column index
	 * @param width - the desired width
	 * @param table - the table
	 */
	public static void setColumnWidth(int columnIndex, int width, JTable table) {
		table.getColumnModel().getColumn(columnIndex).setMinWidth(width);
		table.getColumnModel().getColumn(columnIndex).setMaxWidth(width);
	}
	
	/**
	 * Centers the contents of a column
	 * @param column - the column
	 * @param table - the table
	 */
	public static void centerColumn(int column, JTable table) {
		alignColumn(column, table, SwingConstants.CENTER);
	}
	
	/**
	 * Right-aligns the contents of a column
	 * @param column - the column
	 * @param table - the table
	 */
	public static void rightAlignColumn(int column, JTable table) {
		alignColumn(column, table, SwingConstants.RIGHT);
	}
	
	/**
	 * Left-aligns the contents of a column
	 * @param column - the column
	 * @param table - the table
	 */
	public static void leftAlignColumn(int column, JTable table) {
		alignColumn(column, table, SwingConstants.LEFT);
	}
	
	private static void alignColumn(int column, JTable table, int alignment) {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(alignment);
		table.getColumnModel().getColumn(column).setCellRenderer(centerRenderer);
	}	
}