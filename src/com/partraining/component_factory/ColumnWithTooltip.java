/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

/**
 * Helper class for a JTable column with a tooltip.
 * The hidden column from the data contains the tooltip.
 * @author mcpar
 *
 */
public class ColumnWithTooltip {
	
	private int column;
	private int hiddenColumn;
	
	/**
	 * Constructor
	 * @param column - the column index
	 * @param hiddenColumn - the hidden column index
	 */
	public ColumnWithTooltip(int column, int hiddenColumn) {
		this.column = column;
		this.hiddenColumn = hiddenColumn;
	}
	
	/**
	 * Get the column index
	 * @return - the column index
	 */
	public int getColumn() {
		return column;
	}
	
	/**
	 * Get the hidden column index
	 * @return - the hidden column index
	 */
	public int getHiddenColumn() {
		return hiddenColumn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + hiddenColumn;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColumnWithTooltip other = (ColumnWithTooltip) obj;
		if (column != other.column)
			return false;
		if (hiddenColumn != other.hiddenColumn)
			return false;
		return true;
	}
}
