/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

import java.awt.Component;
import java.awt.Point;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

/**
 * The dialog template
 * @author mcpar
 *
 * @param <T> - data needed for creating the dialog
 */
public abstract class DialogTemplate<T> extends JDialog {

	private static final long serialVersionUID = -5069573402104853199L;
	private transient T data;
	
	/**
	 * Builds dialog with title
	 * @param appTitle - the app title
	 * @param appImageURL - the app image icon URL
	 * @param dialogTitle - the dialog title
	 * @param dialogParent - the parent component
	 * @param isModal - true is modal
	 */
	protected DialogTemplate(String appTitle, URL appImageURL, String dialogTitle, 
			Component dialogParent, boolean isModal) {
		buildUI(appTitle, dialogTitle, dialogParent, isModal, appImageURL);
	}
	
	/**
	 * Builds dialog with title
	 * @param appImageURL - the app image icon URL
	 * @param dialogTitle - the dialog title
	 * @param dialogParent - the parent component
	 * @param isModal - true is modal
	 */
	protected DialogTemplate(URL appImageURL, String dialogTitle, 
			Component dialogParent, boolean isModal) {
		buildUI(dialogTitle, dialogParent, isModal, appImageURL);
	}
	
	/**
	 * Builds dialog with title
	 * @param appTitle - the app title
	 * @param appImageURL - the app image icon URL
	 * @param dialogTitle - the dialog title
	 * @param dialogParent - the parent component
	 * @param isModal - true is modal
	 * @param data - data needed for the dialog during creation
	 */
	protected DialogTemplate(String appTitle, URL appImageURL, String dialogTitle, 
			Component dialogParent, boolean isModal, T data) {
		this.data = data;
		buildUI(appTitle, dialogTitle, dialogParent, isModal, appImageURL);
	}
	
	/**
	 * Builds dialog with title
	 * @param appImageURL - the app image icon URL
	 * @param dialogTitle - the dialog title
	 * @param dialogParent - the parent component
	 * @param isModal - true is modal
	 * @param data - data needed for the dialog during creation
	 */
	protected DialogTemplate(URL appImageURL, String dialogTitle, 
			Component dialogParent, boolean isModal, T data) {
		this.data = data;
		buildUI(dialogTitle, dialogParent, isModal, appImageURL);
	}
	
	/**
	 * Build the content panel.
	 * @return - JPanel containing the content.
	 */
	protected abstract JPanel createContentPanel();
	
	/**
	 * Build menu bar.
	 * @return - JMenuBar or null if menu bar is not needed.
	 */
	protected abstract JMenuBar createMenuBar();
	
	/**
	 * Set the location of the dialog.
	 * Will call pack.
	 * 
	 * @param dialogParent - the parent
	 */
	public void setLocation(Component dialogParent) {
		if (dialogParent != null) {
			this.pack();
			Point parentLocation = dialogParent.getLocation();
			int parentWidth = dialogParent.getSize().width;
			int thisWidth = this.getSize().width;
			int offset = (parentWidth - thisWidth) / 2;
			Point location = new Point();
			location.y = parentLocation.y;
			location.x = parentLocation.x + offset;
			this.setLocation(location);	
		}
	}
	
	protected void buildUI(String appTitle, String dialogTitle, Component dialogParent, 
			boolean isModal, URL appImageURL) {
		String title = appTitle + (dialogTitle != null ? " - " + dialogTitle : "");
		buildUI(title, dialogParent, isModal, appImageURL);
	}
	
	protected void buildUI(String dialogTitle, Component dialogParent, 
			boolean isModal, URL appImageURL) {
		this.setTitle(dialogTitle);		
		this.setModal(isModal);
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		JMenuBar menuBar = createMenuBar();
		if (menuBar != null) {
			setJMenuBar(menuBar);	
		}
		
		this.getContentPane().add(createContentPanel());
		this.setLocation(dialogParent);

		try {
			this.setIconImage(ImageIO.read(appImageURL));
		} catch (IOException e1) {
			//NO OP
		}	
	}
	
	/**
	 * Get the data.
	 * @return - the data
	 * @throws - IllegalStateException if data is null
	 */
	public T getData() {
		if (data == null) {
			throw new IllegalStateException("data is null");
		}
		
		return data;
	}
	
	/**
	 * Close the dialog
	 */
	protected void closeForm() {
		dispose();
	}
}
