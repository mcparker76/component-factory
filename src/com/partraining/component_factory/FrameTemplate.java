/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

/**
 * The frame template
 * @author mcpar
 *
 */
public abstract class FrameTemplate extends JFrame {

	private static final long serialVersionUID = 5651834372663560948L;
	private final String appName;
	private final URL image;
	
	/**
	 * Constructor
	 * @param appName - the app name
	 * @param image - image icon
	 */
	protected FrameTemplate(String appName, URL image) {
		this.appName = appName;
		this.image = image;
		createGUI();
	}
	
	/**
	 * Create the GUI
	 */
	protected void createGUI() {
		setTitle(appName);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		
		try {
			if (image != null) {
				setIconImage(ImageIO.read(image));
			}
		} catch (IOException e) {
			//NO OP
		}
		
		setResizable(false);
		
		setJMenuBar(createMenuBar());
		setContentPane(createContentPane());
		pack();
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width / 8 - this.getSize().width / 8, dim.height / 2 - this.getSize().height / 2);
	}
	
	/**
	 * Create menu bar
	 * @return - JMenuBar
	 */
	protected abstract JMenuBar createMenuBar();
	
	/**
	 * Create main content panel
	 * @return - JPanel
	 */
	protected abstract JPanel createContentPane();

}
