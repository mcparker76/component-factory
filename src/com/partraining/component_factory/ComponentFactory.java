/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.component_factory;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Factory class for creating components
 * @author Matt Parker
 *
 */
public class ComponentFactory {
	//TODO MCP 12-10-2022 separate out widgets to own factories
	private ComponentFactory() {
		//No implementation
	}
	
	/**
	 * Create a button
	 * @param text - the text to display
	 * @return - button
	 */
	public static JButton createButton(String text) {
		return new JButton(text);
	}
	
	/**
	 * Create a button
	 * @param text - the text to display
	 * @param action - action to perform
	 * @return - button
	 */
	public static JButton createButton(String text, Runnable action) {
		JButton button = new JButton(text);
		button.addActionListener(e-> action.run());
		return button;
	}
	
	/**
	 * Create a check box
	 * @param text - the text to display
	 * @param isSelected - is the check box selected?
	 * @return - check box
	 */
	public static JCheckBox createCheckBox(String text, boolean isSelected) {
		return createCheckBox(text, isSelected, null);
	}
	
	/**
	 * Create a check box with action listener
	 * @param text - the text to display
	 * @param isSelected - is the check box selected?
	 * @param action - action to perform
	 * @return - check box
	 */
	public static JCheckBox createCheckBox(String text, boolean isSelected, ActionListener listener) {
		JCheckBox box = new JCheckBox(text);
		box.setSelected(isSelected);
		box.addActionListener(listener);
		return box;
	}
	
	/**
	 * Create a combo box
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox() {
		return new JComboBox<>();
	}
	
	/**
	 * Create a combo box with specified width.
	 * @param width - the desired width
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(int width) {
		JComboBox<T> combo = new JComboBox<>();
		combo.setPreferredSize(new Dimension(width, combo.getPreferredSize().height));
		return combo;
	}
	
	/**
	 * Create a combo box with specified width.
	 * @param width - the desired width
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(int width, ItemListener listener) {
		JComboBox<T> combo = new JComboBox<>();
		combo.setPreferredSize(new Dimension(width, combo.getPreferredSize().height));
		combo.addItemListener(listener);
		return combo;
	}
	
	/**
	 * Create a combo box with an array of values
	 * @param values - the array of values
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(T[] values){
		JComboBox<T> combo = new JComboBox<>();
		for (T value : values) {
			combo.addItem(value);
		}
		
		return combo;
	}
	
	/**
	 * Create a combo box with an array of values
	 * @param values - the array of values
	 * @param width - the desired width
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(T[] values, int width){
		JComboBox<T> combo = new JComboBox<>();
		for (T value : values) {
			combo.addItem(value);
		}
		
		combo.setPreferredSize(new Dimension(width, combo.getPreferredSize().height));
		
		return combo;
	}
	
	/**
	 * Create a combo box with an array of values
	 * @param values - the array of values
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(List<T> values){
		JComboBox<T> combo = new JComboBox<>();
		for (T value : values) {
			combo.addItem(value);
		}
		
		return combo;
	}
	
	/**
	 * Create a combo box with an array of values
	 * @param values - the array of values
	 * @param width - the desired width
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(List<T> values, int width){
		JComboBox<T> combo = new JComboBox<>();
		for (T value : values) {
			combo.addItem(value);
		}
		
		combo.setPreferredSize(new Dimension(width, combo.getPreferredSize().height));
		
		return combo;
	}
	
	/**
	 * Create a combo box with an array of values
	 * @param values - the array of values
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(T[] values, ItemListener listener){
		JComboBox<T> combo = new JComboBox<>();
		for (T value : values) {
			combo.addItem(value);
		}
		
		combo.addItemListener(listener);
		
		return combo;
	}
	
	/**
	 * Create a combo box with an array of values
	 * @param values - the array of values
	 * @return - combo box
	 */
	public static <T> JComboBox<T> createComboBox(List<T> values, ItemListener listener){
		JComboBox<T> combo = new JComboBox<>();
		for (T value : values) {
			combo.addItem(value);
		}
		
		combo.addItemListener(listener);
		
		return combo;
	}
	
	/**
	 * Create a formatted numeric text field
	 * 
	 * @param digits - maximum number of digits
	 * @return - formatted text field
	 * @throws - IllegalArgumentException for less than 1 digit
	 */
	public static JTextField createNumericTextField(int digits) {

		if (digits < 1) {
			throw new IllegalArgumentException("digits is less than 1");
		}

		JTextField field = new JTextField(digits);

		((AbstractDocument) field.getDocument()).setDocumentFilter(new DocumentFilter() {
			Pattern regEx = Pattern.compile("\\d*");

			@Override
			public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
					throws BadLocationException {
				if (offset + 1 > digits) {
					return;
				}

				Matcher matcher = regEx.matcher(text);
				if (!matcher.matches()) {
					return;
				}
				super.replace(fb, offset, length, text, attrs);
			}
		});

		return field;
	}
	
	/**
	 * Create a formatted alphanumeric text field
	 * 
	 * @param digits - maximum number of characters
	 * @return - formatted text field
	 */
	public static JTextField createAlphaTextField(int digits) {

		JTextField field = new JTextField((int)(digits * 0.66));

		((AbstractDocument) field.getDocument()).setDocumentFilter(new DocumentFilter() {
			Pattern regEx = Pattern.compile(".");

			@Override
			public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
					throws BadLocationException {
				if (offset + 1 > digits) {
					return;
				}

				Matcher matcher = regEx.matcher(text);
				if (!matcher.matches()) {
					return;
				}
				super.replace(fb, offset, length, text, attrs);
			}
		});

		return field;
	}
	
	/**
	 * Create a menu
	 * @param menuName - the name of the menu
	 * @return - JMenu
	 */
	public static JMenu createMenu(String menuName) {
		return new JMenu(menuName);
	}
	
	/**
	 * Creates menu bar
	 * @param menus- List<JMenu> menus
	 * @return - JMenuBar
	 */
	public static JMenuBar createMenuBar(List<JMenu> menus) {
		JMenuBar menuBar = new JMenuBar();
		menus.forEach(menu -> menuBar.add(menu));
		return menuBar;
		
	}

	/**
	 * Create a menu item with icon
	 * @param text - the displayed text
	 * @param resourceLoader - the ClassLoader
	 * @param icon - the icon
	 * @param action - action to perform
	 * @return - menu item
	 */
	public static JMenuItem createMenuItem(String text, ClassLoader resourceLoader, String icon, Runnable action) {
		JMenuItem menuItem = new JMenuItem(text);
		
		if (action != null) {
			menuItem.addActionListener(e-> action.run());
		}
		
		if (resourceLoader != null && icon != null) {
			menuItem.setIcon(new ImageIcon(resourceLoader.getResource(icon)));		
		}
		return menuItem;
	}
	
	/**
	 * Create a menu item with icon
	 * @param text - the displayed text
	 * @param resourceLoader - the ClassLoader
	 * @param icon - the icon
	 * @return - menu item
	 */
	public static JMenuItem createMenuItem(String text, ClassLoader resourceLoader, String icon) {
		JMenuItem menuItem = new JMenuItem(text);
		if (resourceLoader != null && icon != null) {
			menuItem.setIcon(new ImageIcon(resourceLoader.getResource(icon)));		
		}
		return menuItem;
	}
	
	/**
	 * Create a menu item with no icon
	 * @param text - the displayed text
	 * @param action - action to perform
	 * @return - menu item
	 */
	public static JMenuItem createMenuItem(String text, Runnable action) {
		return createMenuItem(text, null, null, action);
	}
	
	/**
	 * Create a menu item with no icon and no action listener
	 * @param text - the displayed text
	 * @return - menu item
	 */
	public static JMenuItem createMenuItem(String text) {
		return createMenuItem(text, null, null, null);
	}
	
	/**
	 * Create a radio button with no action listener
	 * @param text - the text to display
	 * @param isSelected - is the button selected?
	 * @return - radio button
	 */
	public static JRadioButton createRadioButton(String text, boolean isSelected) {
		return createRadioButton(text, isSelected, null);
	}
	
	/**
	 * Create a radio button with action listener
	 * @param text - the text to display
	 * @param isSelected - is the button selected?
	 * @param action - action to perform
	 * @return - radio button
	 */
	public static JRadioButton createRadioButton(String text, boolean isSelected, Runnable action) {
		JRadioButton button = new JRadioButton(text);
		if (action != null) {
			button.addActionListener(e -> action.run());
		}
		button.setSelected(isSelected);
		return button;
	}
	
	/**
	 * Create a scroll pane for a component
	 * @param width - width
	 * @param height - height
	 * @param component - the component
	 * @return - scroll pane
	 */
	public static JScrollPane createScrollPane(int width, int height, Component component) {
		JScrollPane pane = new JScrollPane();
		pane.setPreferredSize(new Dimension(width, height));
		pane.setViewportView(component);
		pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		return pane;
	}
	
	/**
	 * Create a tabbed pane
	 * @param position - the JTabbedPane position
	 * @return - JTabbedPane
	 */
	public static JTabbedPane createTabbedPane(int position) {
		JTabbedPane pane = new JTabbedPane(position);
		pane.setBorder(new EmptyBorder(5, 5, 5, 5));
		return pane;
	}
	
	/**
	 * Create text field with specified width
	 * @param columns - number of columns
	 * @return - text field
	 */
	public static JTextField createTextField(int columns) {
		JTextField field = new JTextField();
		field.setColumns(columns);
		return field;
	}
	
	/**
	 * Create text field with specified width
	 * @param columns - number of columns
	 * @param text - text to display
	 * @param enabled - true is enabled
	 * @return - text field
	 */
	public static JTextField createTextField(int columns, String text, boolean enabled) {
		JTextField field = new JTextField();
		field.setColumns(columns);
		field.setText(text);
		field.setEnabled(enabled);
		return field;
	}
	
	/**
	 * Create text field with specified width
	 * @param columns - number of columns
	 * @param horizontalAlignment - use JTextField constants
	 * @param text - text to display
	 * @param enabled - true is enabled
	 * @return - text field
	 */
	public static JTextField createTextField(int columns, int horizontalAlignment,
			String text, boolean enabled) {
		
		JTextField field = createTextField(columns, text, enabled);
		field.setHorizontalAlignment(horizontalAlignment);
		return field;
	}
	
	/**
	 * Create text field with specified width
	 * @param columns - number of columns
	 * @param enabled - true is enabled
	 * @return - text field
	 */
	public static JTextField createTextField(int columns, boolean enabled) {
		JTextField field = new JTextField();
		field.setColumns(columns);
		field.setEnabled(enabled);
		return field;
	}
	
	/**
	 * Create an editor pane to display text containing hyperlinks
	 * @param text - the text to display
	 * @return - the editor pane
	 */
	public static JEditorPane createEditorPane(String text) {
		JEditorPane jep = new JEditorPane("text/html", text);
		
		jep.setEditable(false);
		jep.setOpaque(false);
		
		jep.addHyperlinkListener(e -> {
			if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType()) && 
					Desktop.isDesktopSupported()) {

				try {
					URI uri = new URI(e.getURL().toString());
					Desktop.getDesktop().browse(uri);
				} catch (Exception ex) {
					// NO OP
				}
			}
		});	
		
		return jep;
	}

	/**
	 * Create an unformatted label
	 * @param text - text to display
	 * @return - the label
	 */
	public static JLabel createLabel(String text) {
		return new JLabel(text);
	}	
}